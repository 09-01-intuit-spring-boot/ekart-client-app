package com.classpathio.client.controller;

import java.time.ZoneId;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/userinfo")

public class UserInfoRestController {
	
	@Autowired
	private OAuth2AuthorizedClientService oAuth2AuthorizedClientService;
	
	@GetMapping
	public Map<String, String> userInfo(OAuth2AuthenticationToken oauth2Token){
		
		OAuth2AuthorizedClient oAuth2AuthorizedClient = oAuth2AuthorizedClientService
																.loadAuthorizedClient(oauth2Token.getAuthorizedClientRegistrationId(), oauth2Token.getPrincipal().getName());
		OAuth2AccessToken accessToken = oAuth2AuthorizedClient.getAccessToken();
		
		Map<String, String> responseMap = new LinkedHashMap<>();
		
		
		responseMap.put("token", accessToken.getTokenValue());
		responseMap.put("exp-time", accessToken.getExpiresAt().atZone(ZoneId.systemDefault()).toLocalDate().toString());
		responseMap.put("iss-time", accessToken.getIssuedAt().atZone(ZoneId.systemDefault()).toLocalDate().toString());
		responseMap.put("scopes", accessToken.getScopes().toString());
		
		return responseMap;
	}

}
